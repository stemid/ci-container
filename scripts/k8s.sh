ctr=$1

test -f kubectl || \
  curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
buildah copy "$ctr" kubectl /usr/bin/kubectl
buildah run "$ctr" chmod 0755 /usr/bin/kubectl

test -f install_kustomize.sh || \
  curl -LO "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"
buildah copy "$ctr" install_kustomize.sh /opt/install_kustomize.sh

buildah run "$ctr" bash /opt/install_kustomize.sh
buildah run "$ctr" cp kustomize /usr/bin/kustomize

buildah run "$ctr" go get github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb
buildah run "$ctr" go get github.com/google/go-jsonnet/cmd/jsonnet
buildah run "$ctr" go get github.com/brancz/gojsontoyaml

curl -s https://get.helm.sh/helm-v3.7.1-linux-amd64.tar.gz | tar -xvzf -
buildah copy "$ctr" linux-amd64/helm /usr/bin/helm
buildah run "$ctr" chmod 0755 /usr/bin/helm
