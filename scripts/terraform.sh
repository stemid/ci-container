ctr=$1

buildah run "$ctr" apk add terraform
curl -sL https://releases.hashicorp.com/terraform/1.0.2/terraform_1.0.2_linux_amd64.zip | \
  zcat > files/terraform
chmod 0755 files/terraform
buildah copy "$ctr" 'files/terraform' '/usr/bin/terraform'
