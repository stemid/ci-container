container=$1

buildah run $container apk -U update
buildah run $container apk --update-cache add curl bash jq yq gettext go git
