# CI Container images

These are my own container images used for CI/CD. Use them if you want, but the reason I have them is because I couldn't find anyone elses image I was comfortable using.

Pushed to [Docker Hub](https://hub.docker.com/r/stemid/ci).

# Build

    $ bash build.sh fedora

The argument fedora or alpine runs the scripts fedora.sh or alpine.sh to make OS-specific setups.

Reason is that Fedora worked best with podman and buildah at this moment. While alpine gives me a much smaller image size.

# Image tags

## buildah

Fedora based buildah environment with podman and buildah commands.

## k8s

Alpine based k8s deployment environment with kubectl and kustomize commands.

## aws

Alpine based image with awscli tools installed.

## Example of testing build locally

See .gitlab-ci.yaml file for more.

    export REGISTRY_TAG=k8s
    bash build.sh alpine
