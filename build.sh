#!/usr/bin/env bash

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

#set -o errexit
tag=$1

test -z "$tag" && exit 1

default_alpine=docker.io/alpine:3.18
default_fedora=registry.fedoraproject.org/fedora-minimal:38

# Determine which image we base our own on.
if [ $tag = 'fedora' ]; then
  from_tag=$default_fedora
elif [ $tag = 'alpine' ]; then
  from_tag=$default_alpine
else
  # Default tag, hopefully never used.
  from_tag=$default_alpine
fi

# Use some environment variables here so we can provide them in CI pipelines.
container=$(buildah from $from_tag)
registry_image=${REGISTRY_IMAGE:-registry.hub.docker.com/stemid/ci}
registry_tag="${VERSION:-latest}-${REGISTRY_TAG:-$tag}"

buildah config --label maintainer="Stefan Midjich <swehack at gmail.com>" $container
buildah config --entrypoint '/bin/bash -l' $container

# Run OS specific buildah commands.
bash "scripts/$tag.sh" $container

# Modify path of default root user
buildah copy $container 'files/path.sh' '/etc/profile.d/10-path.sh'
buildah copy $container 'files/.bashrc' '/root/.bashrc'
buildah copy $container 'files/.bashrc' '/root/.profile'

# Run custom tasks for selected registry tag, if any.
test -f "scripts/$REGISTRY_TAG.sh" && bash "scripts/$REGISTRY_TAG.sh" $container

echo "$0: buildah commit"
buildah commit "$container" "ci:$registry_tag"

echo "$0: buildah push to docker://$registry_image:$registry_tag"
